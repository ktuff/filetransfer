# FileTransfer

A small Gui-Application for transferring larger files over the internet. Written with Qt5 and C++.

## Usage
To transfer a file the sender has to change the mode to "upload" and the receiver set it to "download".
Both have to fill in a common port number and the sender needs the target ip address. If the receiver leaves the file field empty, then the original file name is used. The receiver must press the start-button first.

## Compiling
```
mkdir -p build &&
cmake -S src -B build &&
cd build &&
make
```

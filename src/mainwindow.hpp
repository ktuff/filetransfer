#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QThread>

#include "transfer.hpp"

QT_BEGIN_NAMESPACE
namespace Ui {
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

    private:
        Ui::MainWindow *ui;
        int m_mode;
        transfer* m_transfer;
        std::atomic<bool> m_ongoing_transfer;
        QThread* m_transfer_thread;


    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();


    public slots:
        void on_button_mode_clicked();
        void on_button_file_clicked();
        void on_button_start_clicked();
        void on_button_stop_clicked();

        void update_ui();
        void end_transfer();
};

#endif // MAINWINDOW_HPP

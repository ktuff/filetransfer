#include "transfer.hpp"

#include <chrono>
#include <fstream>
#include <thread>

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/poll.h>

transfer::transfer(const std::string& file, const std::string& ip, int port, int mode) :
    QObject(nullptr),
    m_file(file),
    m_ip(ip),
    m_port(port),
    m_mode(mode)
{
}

transfer::~transfer()
{
}


int transfer::get_progress() const
{
    return m_progress;
}

QString transfer::get_state() const
{
    return m_state;
}

void transfer::process()
{
    this->m_progress = 0;

    int rc = 1;
    if(this->m_mode == MODE_DOWNLOAD) {
        this->m_state = QString("Waiting");
        emit update_signal();
        rc = file_recv(this->m_file, this->m_port);
    } else if(this->m_mode == MODE_UPLOAD) {
        rc = file_send(this->m_file, this->m_ip, this->m_port);
    }
    if(rc) {
        this->m_state = QString("Transfer failed");
        this->m_progress = 0;
        emit update_signal();
    } else {
        this->m_state = QString("Transfer successful");
        emit update_signal();
    }
    emit complete();
    emit end_reached();
}

int transfer::file_send(const std::string& filename, const std::string& ip, int port)
{
    int m_socket;
    if((m_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        m_socket = -1;
        return 1;
    }
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    if(inet_pton(AF_INET, ip.c_str(), &address.sin_addr) <= 0) {
        m_socket = -1;
        return 1;
    }

    if(::connect(m_socket, (sockaddr*)&address, sizeof(address)) < 0) {
        m_socket = -1;
        return 1;
    }

    this->m_state = QString("Transfer started");
    emit update_signal();

    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    memcpy(buffer, filename.c_str(), std::min(strlen(filename.c_str()), BUFFER_SIZE));
    send(m_socket, buffer, BUFFER_SIZE, 0);

    std::ifstream file;
    file.open(filename, std::ios::binary);
    if(file.fail() || file.eof()) {
        return 1;
    }
    file.ignore(std::numeric_limits<std::streamsize>::max());
    long file_size = file.gcount();
    file.clear();
    file.seekg(std::ios::beg);
    long progress = 0;

    memset(buffer, 0, sizeof(buffer));
    memcpy(buffer, &file_size, sizeof(long));
    send(m_socket, buffer, BUFFER_SIZE, 0);

    int oldpercent = -1;

    while(!file.eof()) {
        size_t pkt_size = BUFFER_SIZE;
        file.read(buffer, BUFFER_SIZE);
        if(!file) {
            pkt_size = file.gcount();
        }
        send(m_socket, buffer, pkt_size, 0);
        progress += pkt_size;

        int newpercent = (int)(progress/(double)file_size*100.0);
        if(newpercent > oldpercent) {
            this->m_progress = newpercent;
            emit update_signal();
            oldpercent = newpercent;
        }
    }
    file.close();
    emit update_signal();

    ::close(m_socket);

    if(progress == file_size) {
        return 0;
    } else {
        return 1;
    }
}

int transfer::file_recv(const std::string& filename, int port)
{
    int m_socket;
    if((m_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return 1;
    }
    int opt;
    if(setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        ::close(m_socket);
        return 1;
    }
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    if(bind(m_socket, (sockaddr*)&address, sizeof(address)) < 0) {
        ::close(m_socket);
        return 1;
    }

    if(listen(m_socket, 1)) {
        ::close(m_socket);
        return 1;
    }
    int sock;
    if((sock = accept(m_socket, NULL, NULL)) < 0) {
        ::close(m_socket);
        return 1;
    }

    this->m_state = QString("Transfer started");
    emit update_signal();

    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    recv(sock, buffer, BUFFER_SIZE, 0);
    std::string path = std::string(buffer);
    if(filename != "") {
        path = filename;
    }

    std::ofstream stream;
    stream.open(path, std::ios::binary);

    memset(buffer, 0, sizeof(buffer));
    recv(sock, buffer, BUFFER_SIZE, 0);
    long file_size = ((long*)buffer)[0];

    int oldpercent = -1;
    long progress = 0;
    size_t pkt_size = BUFFER_SIZE;
    while(!stream.eof()) {
        pkt_size = recv(sock, buffer, BUFFER_SIZE, 0);
        if(pkt_size > 0) {
            stream.write(buffer, pkt_size);
            progress += pkt_size;
            int newpercent = (int)(progress/(double)file_size*100.0);
            if(newpercent > oldpercent) {
                this->m_progress = newpercent;
                emit update_signal();
                oldpercent = newpercent;
            }
        }
        if(progress >= file_size) {
            break;
        }
    }
    stream.close();
    emit update_signal();

    ::close(sock);
    ::close(m_socket);

    if(progress == file_size) {
        return 0;
    } else {
        return 1;
    }
}

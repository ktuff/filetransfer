#ifndef TRANSFER_HPP
#define TRANSFER_HPP

#include <QObject>
#include <QString>

#define MODE_UPLOAD     0
#define MODE_DOWNLOAD   1

#define BUFFER_SIZE 1024ul

class transfer : public QObject {
    Q_OBJECT

    private:
        const std::string m_file;
        const std::string m_ip;
        const int m_port;
        const int m_mode;

        QString m_state;
        int m_progress;

    public:
        transfer(const std::string& file, const std::string& ip, int port, int mode);
        ~transfer();


    public:
        QString get_state() const;
        int get_progress() const;

    public slots:
        void process();

    signals:
        void update_signal();
        void complete();
        void end_reached();

    private:
        int file_send(const std::string& path, const std::string& ip, int port);
        int file_recv(const std::string& path, int port);
};

#endif // TRANSFER_HPP

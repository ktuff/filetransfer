#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

#include <QFileDialog>

#include <fstream>

#include <pwd.h>
#include <unistd.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_transfer(nullptr),
    m_transfer_thread(nullptr)
{
    ui->setupUi(this);

    this->m_ongoing_transfer = false;
    this->m_mode = MODE_DOWNLOAD;
    this->ui->button_mode->setText(QString("Mode: Download"));
}

MainWindow::~MainWindow()
{
    if(this->m_ongoing_transfer) {
        this->m_transfer_thread->quit();
        this->m_transfer_thread->wait();
    }
    delete ui;
}


void MainWindow::on_button_mode_clicked()
{
    if(m_mode == MODE_DOWNLOAD) {
        this->m_mode = MODE_UPLOAD;
        this->ui->button_mode->setText(QString("Mode: Upload"));
    } else {
        this->m_mode = MODE_DOWNLOAD;
        this->ui->button_mode->setText(QString("Mode: Download"));
    }
}

void MainWindow::on_button_file_clicked()
{
    struct passwd *pw = getpwuid(getuid());
    std::string home_dir = pw->pw_dir;
    QFileDialog dialog(this, QString("Select File"), QString(home_dir.c_str()));
    dialog.exec();
    const auto& files = dialog.selectedFiles();
    if(!files.empty()) {
        ui->edit_file->setText(dialog.selectedFiles().first());
    } else {
        ui->edit_file->setText(QString(""));
    }
}

void MainWindow::on_button_start_clicked()
{
    if(this->m_ongoing_transfer) {
        return;
    }
    std::string file = ui->edit_file->text().toStdString();
    std::string ip = ui->edit_ip->text().toStdString();
    int port = std::stoi(ui->edit_port->text().toStdString());

    this->m_transfer_thread = new QThread(this);
    this->m_transfer = new transfer(file, ip, port, m_mode);
    this->m_transfer->moveToThread(this->m_transfer_thread);
    connect(this->m_transfer_thread, SIGNAL(started()), this->m_transfer, SLOT(process()));
    connect(this->m_transfer, SIGNAL(update_signal()), this, SLOT(update_ui()));
    connect(this->m_transfer, SIGNAL(complete()), this, SLOT(end_transfer()));
    connect(this->m_transfer, SIGNAL(end_reached()), this->m_transfer_thread, SLOT(quit()));
    connect(this->m_transfer_thread, SIGNAL(finished()), this->m_transfer_thread, SLOT(deleteLater()));
    ui->button_start->setDisabled(true);
    this->m_ongoing_transfer = true;
    this->m_transfer_thread->start();
}

void MainWindow::on_button_stop_clicked()
{
    if(this->m_ongoing_transfer) {
        ui->button_start->setDisabled(false);
        ui->edit_error->setText("Aborted");
        this->m_transfer_thread->quit();
        this->m_ongoing_transfer = false;
    }
}

void MainWindow::update_ui()
{
    QString state = this->m_transfer->get_state();
    int progress = this->m_transfer->get_progress();
    ui->edit_error->setText(state);
    ui->bar_progress->setValue(progress);
}

void MainWindow::end_transfer()
{
    QString state = this->m_transfer->get_state();
    ui->edit_error->setText(state);
    ui->button_start->setDisabled(false);
    this->m_ongoing_transfer = false;
}
